import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from ganRNN import GAN
# from netDeCNN import Net as DeCNN
# from netCNN import Net as CNN
# from net3 import Net as Net3
# from net2 import Net
from esn_ip import ESNIP as Reservoir
import matplotlib.pyplot as plt

from loadmusic import *
musicPath = "/home/koloumpras/Development/MusicDatasets/"

datasets = [
            "JSB_Chorales.pickle",
            "MuseData.pickle",
            "Nottingham.pickle",
            "Piano_midi.pickle"
            ]


def print_greyscale(pixels, width=28, height=28):
    def get_single_greyscale(pixel):
        val = 232 + round(pixel * 23)
        return '\x1b[48;5;{}m \x1b[0m'.format(int(val))

    for l in range(height):
        line_pixels = pixels[l * width:(l+1) * width]
        print(''.join(get_single_greyscale(p) for p in line_pixels))


if __name__ =="__main__":
    dataset = datasets[2]

    data = loadMusic(musicPath+dataset)
    train = parseToSparse(data["train"])
    valid = parseToSparse(data["valid"])
    test = parseToSparse(data["test"])
    # import pdb; pdb.set_trace()

    in_dim = 87
    out_dim = in_dim
    in_g = 32

    # minibatch size
    print "Compiling ... ",

    # generator = DeCNN(in_g,in_dim,2048,64,m)
    # discriminator = CNN(in_dim,1,1024,32,m)
    # discriminator = Net3(in_dim,1,240,120,25,10,m)
    # generator = Net(in_g,in_dim,1024,m)
    # discriminator = Net(in_dim,1,784*2,m)
    generator = Reservoir(in_g,512,out_dim)
    discriminator = Reservoir(in_dim,1024,1,output_sparsity=.01)

    gan = GAN(generator,discriminator)
    gan.trainD()
    gan.trainG()
    gan.stepD()
    gan.stepG()
    print " ... done"

    # lol
    inp = train
    # import pdb;pdb.set_trace()
    inp_shape = inp[0].shape[0]
    m = inp_shape

    print "Pre-trainig D ..."
    for _ in range(0):
        for r_id, roll in enumerate(inp):
            print r_id
            x = roll
            m = roll.shape[0]
            # sample m examples of z
            z = np.random.random(
                    (m,in_g)
                    ).astype(theano.config.floatX)
            # sample m examples from data
            # s_x = np.random.randint(size=(m), low=0,
            #                 high=len(inp))
            d_er = gan.trainerD(x,z,1e-4)
        if _ % 10 == 0:
            print "Epc ", _, " loss ", d_er.mean()
            print_greyscale(g[-1],width=10,height=9)
    print "... one"


    # steps to train the D more
    k_steps = 1

    # 100 epochs over the data
    epochs = 100
    lr = 5e-4

    print "Testing ... "
    derrrs = []
    gerrrs = []

    for i in xrange(epochs):
        for roll in inp[:1]:
            # import pdb; pdb.set_trace()
            x = roll
            m = roll.shape[0]
            # sample m examples from z1
            z1 = np.random.random(
                (m,in_g)
                ).astype(theano.config.floatX)
            g_er = gan.trainerG(z1,lr)

            for ks in xrange(k_steps):
                # sample m examples of z
                z = np.random.random(
                    (m,in_g)
                    ).astype(theano.config.floatX)
                # sample m examples from data
                # s_x = np.random.randint(size=(m), low=0,
                                # high=len(inp))
                # x = inp[s_x]
                d_er = gan.trainerD(x,z,lr)


            print_greyscale(gan.stepperG(z1)[-1],width=10,height=9)
            print gan.stepperD(gan.stepperG(z1))[-1]

            # gerrrs.append(g_er.mean())
            # derrrs.append(d_er.mean())
            # plt.ion()
            # plt.clf()
            # plt.plot(gerrrs)
            # plt.plot(derrrs)
            # plt.draw()


            if (i+1)%10 == 0:
                gan.save(i)
                
                print "####EPOCH ",i,"####"
                print "G: Error ",g_er.mean()," at G "
                print "D: Error ",d_er.mean()," at D "

                test_range = 1
                gens = []
                des = []
                for t in xrange(test_range):
                    z = np.random.random(
                            (m,in_g)
                            ).astype(theano.config.floatX)
                    gens.append(gan.stepperG(z)[-1])
                    des.append(gan.stepperD(gan.stepperG(z))[-1])

                print "Generated"
                for g,d  in zip(gens,des):
                    print "@@@"
                    print_greyscale(g,width=10,height=9)
                    print "This comes from the dataset, ",d[0]*100,"%! LOL"
'''
# z = np.random.multivariate_normal(
#                     mean,cov,(m)
#                     ).astype(theano.config.floatX)
'''
