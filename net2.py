import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

class Net():
	def __init__(self, indim,outdim,hdim, b_size,
	 			scale = .001,scale_b=.001,softmax=False):
		self.inp_s = indim
		self.out_s = outdim
		self.hid_s = hdim

		self.weights_ih = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.inp_s)
				-1,
				1,
				(self.inp_s, self.hid_s)
				).astype(theano.config.floatX),
				name="weights_ih"
			)

		self.weights_ho = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s, self.out_s)
				).astype(theano.config.floatX),
				name="weights_ho"
			)

		self.weights_bias_ho = theano.shared(
			scale_b*np.ones((b_size,self.out_s)
				).astype(theano.config.floatX),
				name="weights_bias_ho"
			)

		self.weights_bias_ih = theano.shared(
			scale_b*np.ones((b_size,self.hid_s)
				).astype(theano.config.floatX),
				name="weights_bias_ih"
			)

		self.lr = TT.scalar(name="learning rate")
		self.E = TT.scalar(name="Error")
		self.inp = TT.matrix(name="input")
		self.target = TT.matrix(name="target")
		if softmax == True:
			self.f_out = TT.nnet.softmax
			self.f_in = TT.nnet.sigmoid
		else:
			self.f_out = TT.nnet.sigmoid
			self.f_in = TT.tanh
        
	def set_params(self,):
		self.params =[
                        self.weights_ih, self.weights_ho,
                        self.weights_bias_ih, self.weights_bias_ho
                    ]
	def step(self, vin, dropout=False):

		inp = vin

		ha = self.f_in(
			TT.dot(inp,self.weights_ih)+self.weights_bias_ih
			)
		oa = self.f_out(TT.dot(ha,self.weights_ho)+self.weights_bias_ho
			)
		return oa
	def step_f(self,):
		output = self.step(self.inp)

		self.stepper = theano.function(
			inputs=[self.inp],
			outputs=output)

		return self.stepper

	def train_f(self,):
		output = self.step(self.inp)
		normalisationTerm = (self.weights_ho.norm(L=1)+self.weights_ih.norm(L=1))
		self.E = TT.mean((self.target - output)**2)


		params =[
			self.weights_ih, self.weights_ho,
			self.weights_bias_ih, self.weights_bias_ho
		]
		self.params = params

		gparams = TT.grad(self.E+normalisationTerm, params)
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E+normalisationTerm, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])

		updates = [
			(param, param - self.lr * gparam)
			for param, gparam in zip(params, gparams)
		]

		self.trainer =  theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[self.E+normalisationTerm],
			updates= updates
			)

		return self.trainer

if __name__ =="__main__":
	import matplotlib.pyplot as plt

	data = np.random.normal(size=(11000,230))
	b_size = 30
	epochs = 100


	print "Compiling.."
	n = Net(230,230,450,b_size)
	n.step_f()
	n.train_f()

	print "Testing.."
	errors = []
	for e in range(epochs):
		np.random.shuffle(data)
		for b_i in range(len(data)/b_size):
			inp = data[b_i*b_size: (b_i+1)*b_size]
			out = n.stepper(inp)

			error = ((out-inp)**2).mean()


			er = n.trainer(inp,out[0],0.01)
			errors.append(er[0])
		print "batch ",b_i," error ", error," epoch ",e

		# plt.plot(errors)
		# plt.show()
