import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from theano.sandbox.cuda.dnn import dnn_conv
from ops import batchnorm, conv_cond_concat, deconv, dropout

class Net():
	def __init__(self, indim,outdim,units,filters, 
			 b_size, condition=0, scale = .01):
		# input size
		self.inp_s = indim
		# output size
		self.out_s = outdim
		# number of units to project the convolutions
		self.units = units
		# number of filter for convolution
		self.filters = filters
		# batch size
		self.batch_size = b_size
		# channels
		chan = 1
		# condition
		self.cond = condition

		if self.cond == 0:
			self.weights_ih = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.inp_s, self.units)
					).astype(theano.config.floatX),
					name="weights_ih"
				)

			self.weights_hh1 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.units, self.filters*2*7*7)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
			self.weights_h1h2 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2, self.filters,5,5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
			self.weights_h2h3 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters, chan,5,5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
		else:
			self.weights_ih = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.inp_s+self.cond, self.units)
					).astype(theano.config.floatX),
					name="weights_ih"
				)

			self.weights_hh1 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.units+self.cond, self.filters*2*7*7)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
			self.weights_h1h2 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2+self.cond, self.filters,5,5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
			self.weights_h2h3 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters+self.cond, chan,5,5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)

		
		

		self.lr = TT.scalar(name="learning rate")
		self.E = TT.scalar(name="Error")
		self.inp = TT.matrix(name="input")
		self.target = TT.matrix(name="target")

	def set_params(self,):
		self.params =[
                        self.weights_ih,
						self.weights_hh1,
						self.weights_h1h2,
						self.weights_h2h3,
						
                    ]

	def step(self, inp, cond=None,dropout=False):

		if self.cond==0 and cond!= None:
			cond = TT.dimshuffle(0,1,'x','x')
			inp = TT.concatenate([inp, cond], axis=1)

		h = TT.nnet.relu(batchnorm(
				TT.dot(inp,self.weights_ih)
				)
			)

		if self.cond==0 and cond!= None:
			inp = TT.concatenate([h, cond], axis=1)

		h1 = TT.nnet.relu(batchnorm(
				TT.dot(h,self.weights_hh1)
				)
			)
		h1 = h1.reshape((h1.shape[0], self.filters*2, 7, 7))
		
		if self.cond==0 and cond!= None:
			h1 = conv_cond_concat(h1, cond)

		h2 = TT.nnet.relu(batchnorm(
				deconv(h1, self.weights_h1h2, subsample=(2, 2), border_mode=(2, 2))
				)
			)

		o = TT.nnet.sigmoid(
				deconv(h2, self.weights_h2h3, subsample=(2, 2), border_mode=(2, 2))
			)
		
		o = o.reshape((self.batch_size,self.out_s))
		return o
	def step_f(self,):
		output = self.step(self.inp)

		self.stepper = theano.function(
			inputs=[self.inp],
			outputs=output)

		return self.stepper

	def train_f(self,):
		output = self.step(self.inp)
		normalisationTerm = (self.weights_ho.norm(L=1)+self.weights_ih.norm(L=1))
		self.E = TT.mean((self.target - output)**2)

		params = self.params
			

		gparams = TT.grad(self.E+normalisationTerm, params)
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E+normalisationTerm, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])

		updates = [
			(param, param - self.lr * gparam)
			for param, gparam in zip(params, gparams)
		]

		self.trainer =  theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[self.E+normalisationTerm],
			updates= updates
			)

		return self.trainer

if __name__ =="__main__":
	import matplotlib.pyplot as plt

	data = np.random.normal(size=(11000,230))
	b_size = 30
	epochs = 100


	print "Compiling.."
	n = Net(230,230,450,b_size)
	n.step_f()
	n.train_f()

	print "Testing.."
	errors = []
	for e in range(epochs):
		np.random.shuffle(data)
		for b_i in range(len(data)/b_size):
			inp = data[b_i*b_size: (b_i+1)*b_size]
			out = n.stepper(inp)

			error = ((out-inp)**2).mean()


			er = n.trainer(inp,out[0],0.01)
			errors.append(er[0])
		print "batch ",b_i," error ", error," epoch ",e

		# plt.plot(errors)
		# plt.show()
