import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from gan import GAN
from netDeCNN import Net as DeCNN
from netCNN import Net as CNN
from net3 import Net as Net3
from net2 import Net 
import matplotlib.pyplot as plt

from mnist import MNIST

def print_greyscale(pixels, width=28, height=28):
    def get_single_greyscale(pixel):
        val = 232 + round(pixel * 23)
        return '\x1b[48;5;{}m \x1b[0m'.format(int(val))

    for l in range(height):
        line_pixels = pixels[l * width:(l+1) * width]
        print(''.join(get_single_greyscale(p) for p in line_pixels))


if __name__ =="__main__":
    mndata = MNIST('./data')
    images, labels = mndata.load_training()

    im_1 = []
    for im,lb in zip(images,labels):
        if lb==2:
            im_1.append(im)

    data = (np.array(images)[:])/255.
    
    in_dim = 784
    out_dim = in_dim
    in_g = 100

    # minibatch size
    m  = 256
    print "Compiling ... ",
    
    generator = DeCNN(in_g,in_dim,2048,64,m)
    discriminator = CNN(in_dim,1,1024,32,m)
    # discriminator = Net3(in_dim,1,240,120,25,10,m)
    # generator = Net(in_g,in_dim,1024,m)
    # discriminator = Net(in_dim,1,784*2,m)
    
    gan = GAN(generator,discriminator)
    gan.trainD()
    gan.trainG()
    gan.stepD()
    gan.stepG()
    print " ... done"

    # lol
    inp = data

    inp_shape = inp[0].shape[0]

    # parameterisation of the noise generator
    mean = np.zeros(in_g)
    cov = np.zeros((in_g,in_g))
    np.fill_diagonal(cov,1.)

    print "Pre-trainig D ...",
    for _ in xrange(000):
        # sample m examples of z
       
        z = np.random.random(
                            (m,in_g)
                            ).astype(theano.config.floatX)
        # sample m examples from data
        s_x = np.random.randint(size=(m), low=0,
                        high=len(inp))
        x = inp[s_x]
        d_er = gan.trainerD(x,z,1e-4)
        if _ %1000 == 0:
            print "Epc ", _, " loss ", d_er.mean()
    print "... one"

    # steps to train the D more
    k_steps = 1

    # 100 epochs over the data
    epochs = 100*data.shape[0]
    lr = 5e-4
    
    print "Testing ... "
    derrrs = []
    gerrrs = []
    
    for i in xrange(epochs):
        for b in xrange(data.shape[0]/m):
            x = data[m*b:m*(b+1)]
            # sample m examples from z1
            z1 = np.random.random(
                            (m,in_g)
                            ).astype(theano.config.floatX)
            g_er = gan.trainerG(z1,lr)

            for ks in xrange(k_steps):
                # sample m examples of z
                z = np.random.random(
                            (m,in_g)
                            ).astype(theano.config.floatX)
                # sample m examples from data
                # s_x = np.random.randint(size=(m), low=0,
                                # high=len(inp))
                # x = inp[s_x]
                d_er = gan.trainerD(x,z,lr)


            # print_greyscale(gan.stepperG(z1)[-1])
            # print gan.stepperD(gan.stepperG(z1))[-1]

            # gerrrs.append(g_er.mean())
            # derrrs.append(d_er.mean())
            # plt.ion()
            # plt.clf()
            # plt.plot(gerrrs)
            # plt.plot(derrrs)
            # plt.draw()


            if b%10 == 0:
                print "####EPOCH ",i,"####"
                print "G: Error ",g_er.mean()," at G "
                print "D: Error ",d_er.mean()," at D "

                test_range = 1
                gens = []
                des = []
                for t in xrange(test_range):
                    z = np.random.random(
                            (m,in_g)
                            ).astype(theano.config.floatX)
                    gens.append(gan.stepperG(z)[0])
                    des.append(gan.stepperD(gan.stepperG(z))[0])

                print "Generated"
                for g,d  in zip(gens,des):
                    print "@@@"
                    print_greyscale(g)
                    print "This comes from the dataset, ",d[0]*100,"%! LOL"

'''
# z = np.random.multivariate_normal(
#                     mean,cov,(m)
#                     ).astype(theano.config.floatX)
'''