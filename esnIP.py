import numpy as np
import theano
import theano.tensor as TT
from theano.ifelse import ifelse
from theano.tensor.shared_randomstreams import RandomStreams
import sys
import theano.sparse as sp
import scipy

np.random.seed(0xbeef)
rng = RandomStreams(seed=np.random.randint(1 << 30))

def r(asd):
	# return TT.clip(asd,-.1,.1)
	return asd#np.tanh(asd)
	# return TT.nnet.sigmoid(asd)
	# return TT.nnet.softmax(asd)[0]#

class ESN():
	def __init__(self,N,K,L,scale,scaleI,scaleFb,alpha,fback = False):
		self.units = N
		self.in_dim = K
		self.out_dim = L
		self.weight_scale = scale

		self.state_z = theano.shared(
			np.zeros(self.units+self.in_dim).astype(theano.config.floatX))
		self.v_n = theano.shared(
			0.001*np.random.random(self.units))
		self.y_init = theano.shared(
			np.zeros(self.out_dim).astype(theano.config.floatX))

		self.input = TT.matrix(name="inputs")
		self.d = TT.matrix(name="teacher")
		self.output = TT.matrix(name="outputs")
		self.coef = TT.scalar()

		self.W_in = theano.shared(
				scaleI*(
					scipy.sparse.rand(
							self.units,
							self.in_dim,
							density=.1)
				).todense().astype(theano.config.floatX)
				)

		self.W_inb = theano.shared(
					0.051*np.ones((1,self.units))
				).flatten()

		W0 = self.weight_scale*(
				scipy.sparse.rand(
							self.units,
							self.units,
							density=.25)
				).todense()
		eigvals = np.linalg.eigvals(W0)
		lamda = np.max(np.absolute(eigvals))
		W1 = (1/np.absolute(lamda)) * W0
		self.W = theano.shared(
			(alpha * W1).astype(theano.config.floatX)
			)

		if fback:
			self.W_fb = theano.shared(
				scaleFb*(
					scipy.sparse.rand(
							self.units,
							self.out_dim,
							density=.1)
				).todense().astype(theano.config.floatX)
				)
		else:
			self.W_fb = theano.shared(
				self.weight_scale*( np.zeros(
					(self.units,self.out_dim)
					).astype(theano.config.floatX))
				)

		self.W_out = theano.shared(
				self.weight_scale*(
					self.get_weights(
						size=(
							self.out_dim,
							self.units+self.in_dim),
						low=-1.,
						high=1.).astype(
						theano.config.floatX))
				)

		self.M = TT.matrix()
		self.T = TT.matrix()
		self.f = TT.tanh#TT.nnet.sigmoid#
		self.inv = TT.nlinalg.MatrixPinv()

		self.g = r
		self.ip_gain= theano.shared(np.ones(self.units))
		self.ip_bias= theano.shared(np.zeros(self.units))
		self.lr = 0.0001
		self.m = 0.
		self.var = np.square(.8)

	def get_weights(self, size, low, high):
		return (high-low)*np.random.random(size)-high
		# return np.random.uniform(size=size, low= low, high=high)

	def compute_state(self,u, t_tm1, state_tm1, y_tm1, W, W_in, W_fb, W_out, c, randomn, W_inb, ip_gain, ip_bias):

			#t-1
			term = TT.dot(W_out,state_tm1)
			#t-1
			y = self.g(term)#TT.nnet.sigmoid(term)
			#t-1
			x = state_tm1[:self.units]
			#t-1
			term1 = TT.dot(W, x)
			#t
			term2 = TT.dot(W_in, u)+ W_inb
			#t-1
			tmp = ifelse(TT.lt(c, 0.5), y_tm1, t_tm1)
			term3 = TT.dot(W_fb, tmp)

			#state with a tad of #random noise, yes random, yes noise#
			term4 = ifelse(TT.lt(c, 0.5), TT.zeros_like(randomn),
						0.001*np.random.random(self.units))
			a = .4
			res_in = term1+term2+term3
			state_x = (1.-a)*state_tm1[:self.units] +\
				a*self.f(self.ip_gain * res_in + self.ip_bias )+term4

			res_out = state_x

			d_ip = (-self.lr) * ((-(self.m / self.var)) + (res_out / self.var) * \
				((2. * self.var) + 1. - np.square(res_out) + self.m * res_out))

			ip_bias += d_ip
			ip_gain += (self.lr / self.ip_gain) + (d_ip * res_in)


			#t for statex, t-1 for u
			state_tm1 = TT.set_subtensor(
				state_tm1[:self.units], state_x)
			#t for statex, t for u
			state_tm1 = TT.set_subtensor(
				state_tm1[self.units:(self.units+self.in_dim)], u)
			#t for output as well
			return state_tm1, y, t_tm1,ip_bias,ip_gain

	def step_taped(self,):
		p = TT.scalar()
		#we have state_z and output for time scale T
		[state_z, output, dinv,ip_bias,ip_gain],_ = theano.scan(self.compute_state,
						sequences=[self.input, dict(input=self.d, taps=[-1])],
						outputs_info=[
							p*self.state_z,
							self.y_init,
							None,
							None,
							None
							],
						non_sequences=[self.W, self.W_in, self.W_fb, self.W_out,
						 	self.coef, self.v_n, self.W_inb,
							self.ip_gain,self.ip_bias]
			)

		return theano.function(
			inputs=[self.input, self.d, self.coef,p],
			outputs=[state_z, output, dinv,ip_bias[-1],ip_gain[-1]],
			updates={(self.state_z,state_z[-1]),
					(self.y_init ,output[-1]),
					(self.ip_bias,ip_bias[-1]),
					(self.ip_gain,ip_gain[-1])})

	def step(self,):
		u = TT.vector()
		#t-1
		term = TT.dot(self.W_out,self.state_z)
		#t-1
		y = self.g(term)#TT.nnet.sigmoid(term)
		#t-1
		x = self.state_z[:self.units]
		#t-1
		term1 = TT.dot(self.W, x)
		#t
		term2 = TT.dot(self.W_in, u)+ self.W_inb
		#t-1
		term3 = TT.dot(self.W_fb, self.y_init)

		#t
		a = .4
		res_in = term1+term2+term3
		state_x = (1.-a)*x + a*self.f(self.ip_gain * res_in + self.ip_bias)

		#t for statex, t-1 for u
		state_z = TT.set_subtensor(
			self.state_z[:self.units], state_x)
		# #t for statex, t for u
		state_z = TT.set_subtensor(
			self.state_z[self.units:(self.units+self.in_dim)], u)

		# output = TT.nnet.softmax(y)[0]
		# output = TT.nnet.sigmoid(y)#/6.
		output = y
		# output -= output.min()
		# output /= output.max()
		# output = TT.clip(y,-.1,.1)


		dinv = y

		return theano.function(
			inputs=[u],
			outputs=[self.state_z, output, dinv],
			updates={(self.state_z,TT.set_subtensor(
							self.state_z[:(self.units+self.in_dim)],
							TT.concatenate([state_x,u]))),
					# (self.state_z,TT.set_subtensor(
					# 		self.state_z[self.units:(self.units+self.in_dim)],
					# 		u)),
					(self.y_init ,output)})

	def train(self,):


		M_tonos = self.inv(self.M)
		# M_tonos = TT.dot(self.inv(TT.dot(self.M.transpose(),self.M)),self.M.transpose())
		all_this = self.T


		#TT.nnet.sigmoid(self.T)*(1- TT.nnet.sigmoid(self.T))
		#self.T
		#1-TT.tanh(self.T)**2
		#self.f(self.T)*(1-self.f(self.T))#
		W_trans = TT.dot(M_tonos,all_this).transpose()
		# self.W_out = TT.set_subtensor(self.W_out[:],W_trans)



		return theano.function(
			inputs=[self.M, self.T],
			outputs=[W_trans],
			updates={(self.W_out, W_trans)}
			)

	def save(self, filename):
		import pickle
		f= open(filename, "wb")
		pickle.dump(
			(
			self.W_in.get_value(),
			self.W.get_value(),
			self.W_fb.get_value(),
			self.W_out.get_value()
			), f)
		f.close()
	def load(self, filename):
		import pickle
		f=open(filename,"rb")
		self.W_in,self.W,self.W_fb,self.W_out  = pickle.load(f)
		f.close()


	# def run(self,):


	# 	def compute_stateR(u, state_tm1,  W, W_in, W_fb, W_out):
	# 		#t-1
	# 		term = TT.dot(state_tm1,W_out)
	# 		#t-1
	# 		y = self.g(term)
	# 		#t-1
	# 		x = state_tm1[:self.units]
	# 		#t-1
	# 		term1 = TT.dot(x, W)
	# 		#t
	# 		term2 = TT.dot(u,W_in)
	# 		#t-1
	# 		term3 = TT.dot(W_fb, y)
	# 		#t
	# 		state_x = self.f(term1+term2+term3)
	# 		#t for statex, t-1 for u
	# 		state_t = TT.set_subtensor(state_tm1[:self.units], state_x)
	# 		#t for statex, t for u
	# 		state_t = TT.set_subtensor(state_tm1[self.units:(self.units+self.in_dim)], u)
	# 		# state_tm1 = TT.set_subtensor(state_tm1[-self.out_dim:], y)
	# 		# theano.printing.Print('this is a very important value')(state_tm1)
	# 		return state_t, y

	# 	# theano.printing.Print('this is a very important value')(self.state_z)
	# 	#we have state_z and output for time scale T
	# 	[state_z, output],_ = theano.scan(compute_stateR,
	# 					sequences=[self.input],
	# 					outputs_info=[
	# 						self.state_z,
	# 						None
	# 						],
	# 					non_sequences=[self.W, self.W_in, self.W_fb, self.W_out]
	# 		)

	# 	#update state value for next iteration
	# 	self.state_z = state_z
	# 	self.output = output

	# 	return theano.function(inputs=[self.input], outputs=[state_z, output])
