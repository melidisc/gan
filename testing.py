import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from gan import GAN
from net2 import Net
from net3 import Net as Net3
import matplotlib.pyplot as plt

from mnist import MNIST

def print_greyscale(pixels, width=28, height=28):
    def get_single_greyscale(pixel):
        val = 232 + round(pixel * 23)
        return '\x1b[48;5;{}m \x1b[0m'.format(int(val))

    for l in range(height):
        line_pixels = pixels[l * width:(l+1) * width]
        print(''.join(get_single_greyscale(p) for p in line_pixels))


if __name__ =="__main__":
    mndata = MNIST('./data')
    images, labels = mndata.load_training()
    im_1 = []
    for im,lb in zip(images,labels):
        if lb==2:
            im_1.append(im)
    data = (np.array(images)[:])/255.
    # data_lbl = np.array([(np.zeros(10)[l] = 1. for l in labels])
    in_dim = 784
    out_dim = in_dim
    in_g = 100
    # data = np.random.normal(size=(11000,in_dim))

    # minibatch size
    m  = 100
    print "Compiling ... ",
    generator = Net3(in_g,in_dim,in_g,1024,1024,1024,m)
    discriminator = Net3(in_dim,1,240,120,25,10,m)
    # generator = Net(in_g,in_dim,1024,m)
    # discriminator = Net(in_dim,1,600,m)
    gan = GAN(generator,discriminator)
    gan.trainD()
    gan.trainG()
    gan.stepD()
    gan.stepG()
    print " ... done"


    # ganTrainer(data)

    inp = data


    inp_shape = inp[0].shape[0]
    print "Pre-trainig D ..."
    mean = np.zeros(in_g)
    cov = np.zeros((in_g,in_g))
    np.fill_diagonal(cov,1.)
    for _ in xrange(10000):
        # sample m examples of z
        z = np.random.multivariate_normal(
                            mean,cov,(m)
                            ).T.astype(theano.config.floatX)
        # z = np.linspace(-in_g,in_g,m*in_g).reshape((m,in_g))
        # sample m examples from data
        s_x = np.random.randint(size=(m), low=0,
                        high=len(inp))
        x = inp[s_x]
        # import pdb; pdb.set_trace()
        d_er = gan.trainerD(x,z,1e-4)
        if _ %1000 == 0:
            print "Epc ", _, " loss ", d_er.mean()
    # optimisation steps for D, one step for G
    k_steps = 1
    epochs = 10000*data.shape[0]
    lr = 1e-4
    print "Testing ... "
    derrrs = []
    gerrrs = []
    
    for i in xrange(epochs):

        for b_i in range(1):#len(data)/m):

            for ks in xrange(k_steps):
                # sample m examples of z
                z = np.random.multivariate_normal(
                            mean,cov,(m)
                            ).T.astype(theano.config.floatX)
                    # -1.,1.,size=(m,in_g)).astype(theano.config.floatX)
                # z = np.linspace(-in_g,in_g,m*in_g).reshape((m,in_g))
                # sample m examples from data
                s_x = np.random.randint(size=(m), low=0,
                                high=len(inp))
                x = inp[s_x]
                # import pdb; pdb.set_trace()
                d_er = gan.trainerD(x,z,lr)

            # sample m examples from z
            # z1 = np.random.uniform(-1.,1.,size=(m,in_g)).astype(theano.config.floatX)
            z1 = np.random.multivariate_normal(
                            mean,cov,(m)
                            ).T.astype(theano.config.floatX)
            g_er = gan.trainerG(z1,lr)



        # print_greyscale(gan.stepperG(z1)[-1])
        # print gan.stepperD(gan.stepperG(z1))[-1]

        # gerrrs.append(g_er.mean())
        # derrrs.append(d_er.mean())
        # plt.ion()
        # plt.clf()
        # plt.plot(gerrrs)
        # plt.plot(derrrs)
        # plt.draw()
        if i%100==0:
            print "####EPOCH ",i,"####"
            print "G: Error ",g_er.mean()," at G "
            print "D: Error ",d_er.mean()," at D "

            test_range = 1
            gens = []
            des = []
            for t in xrange(test_range):
                z = np.random.multivariate_normal(
                            mean,cov,(m)
                            ).T.astype(theano.config.floatX)
                gens.append(gan.stepperG(z)[m/2])
                des.append(gan.stepperD(gan.stepperG(z))[m/2])

                # z = np.random.random(size=(m,in_g))
                # gens.append(gan.stepperG(z)[-1])
                # des.append(gan.stepperD(gan.stepperG(z))[-1])

            print "Generated"
            for g,d  in zip(gens,des):
                print "@@@"
                print_greyscale(g)
                print d

    import pdb; pdb.set_trace()
                # print " @@@ rand"
                # print_greyscale(gan.stepperG(np.ones_like(z)*np.random.random())[0])
