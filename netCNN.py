import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from theano.sandbox.cuda.dnn import dnn_conv
from ops import batchnorm, conv_cond_concat, deconv, dropout

class Net():
	def __init__(self, indim,outdim,units,filters, 
			b_size, condition=0, scale = .01):
		# input size
		self.inp_s = indim
		# output size
		self.out_s = outdim
		# number of units to project the convolutions
		self.units = units
		# number of filter for convolution
		self.filters = filters
		# batch size
		self.batch_size = b_size
		# input channels
		inp_chan = 1
		# condition
		self.cond = condition

		if self.cond == 0:
			# filter #1 (5x5)
			self.weights_ih = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters, inp_chan, 5, 5)
					).astype(theano.config.floatX),
					name="weights_ih"
				)

			# filter #1 (5x5)
			self.weights_hh1 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2, self.filters, 5, 5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)

			# project to units
			self.weights_h1h2 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2*7*7, self.units)
					).astype(theano.config.floatX),
					name="weights_ho"
				)

			# last layer to output
			self.weights_h2h3 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.units, self.out_s)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
		else:
			# filter #1 (5x5)
			self.weights_ih = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters, inp_chan+self.cond, 5, 5)
					).astype(theano.config.floatX),
					name="weights_ih"
				)

			# filter #1 (5x5)
			self.weights_hh1 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2, self.filters+self.cond, 5, 5)
					).astype(theano.config.floatX),
					name="weights_ho"
				)

			# project to units
			self.weights_h1h2 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.filters*2*7*7+self.cond, self.units)
					).astype(theano.config.floatX),
					name="weights_ho"
				)

			# last layer to output
			self.weights_h2h3 = theano.shared(
				np.random.normal(
					0,
					scale,
					(self.units+self.cond, self.out_s)
					).astype(theano.config.floatX),
					name="weights_ho"
				)
		

		
		
		# learning rate
		self.lr = TT.scalar(name="learning rate")
		# error
		self.E = TT.scalar(name="Error")
		# input
		self.inp = TT.tensor4(name="input")

	def set_params(self,):

		# model parameters, easier to update
		self.params =[
                        self.weights_ih,
						self.weights_hh1,
						self.weights_h1h2,
						self.weights_h2h3,
						
                    ]

	def step(self, inp, dropout=False):

		# reshape the input for convolutions
		inp = inp.reshape((self.batch_size,28,28))
		inp = inp.dimshuffle(0,'x',1,2)

		# first conv layer
		h = TT.nnet.relu(
				dnn_conv(inp, self.weights_ih, 
					subsample=(2, 2), border_mode=(2, 2),
					algo="fft")
			)

		# second conv layer
		h1 = TT.nnet.relu(batchnorm(
				dnn_conv(h, self.weights_hh1, 
					subsample=(2, 2), border_mode=(2, 2),
					algo="fft")
				)
			)
		h1 = TT.flatten(h1,2)

		# projection
		h2 = TT.nnet.relu(batchnorm(
				TT.dot(h1,self.weights_h1h2)				
				)
			)

		# combination for output
		o = TT.nnet.sigmoid(
				TT.dot(h2, self.weights_h2h3)
			)
		# reshape the output to proper output
		# o = o.reshape((self.batch_size,self.out_s))
		
		return o
	def step_f(self,):
		output = self.step(self.inp)

		self.stepper = theano.function(
			inputs=[self.inp],
			outputs=output)

		return self.stepper

	def train_f(self,):
		output = self.step(self.inp)
		normalisationTerm = (self.weights_ho.norm(L=1)+self.weights_ih.norm(L=1))
		self.E = TT.mean((self.target - output)**2)

		params = self.params
			

		gparams = TT.grad(self.E+normalisationTerm, params)
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E+normalisationTerm, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])

		updates = [
			(param, param - self.lr * gparam)
			for param, gparam in zip(params, gparams)
		]

		self.trainer =  theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[self.E+normalisationTerm],
			updates= updates
			)

		return self.trainer

if __name__ =="__main__":
	import matplotlib.pyplot as plt

	data = np.random.normal(size=(11000,230))
	b_size = 30
	epochs = 100


	print "Compiling.."
	n = Net(230,230,450,b_size)
	n.step_f()
	n.train_f()

	print "Testing.."
	errors = []
	for e in range(epochs):
		np.random.shuffle(data)
		for b_i in range(len(data)/b_size):
			inp = data[b_i*b_size: (b_i+1)*b_size]
			out = n.stepper(inp)

			error = ((out-inp)**2).mean()


			er = n.trainer(inp,out[0],0.01)
			errors.append(er[0])
		print "batch ",b_i," error ", error," epoch ",e

		# plt.plot(errors)
		# plt.show()
