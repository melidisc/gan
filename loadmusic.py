import cPickle
import numpy as np
import theano
import pygame

def loadMusic(path):
	dataset = cPickle.load(file(path))
	return dataset

def parseToSparse(data):

	new_data = []
	for l in data:
		new_l = []
		for roll in l:
			a = np.zeros((108-21)).astype(theano.config.floatX)
			for note in roll:
				a[note-21] = 1.
			new_l.append(a.astype(theano.config.floatX))
		new_data.append(np.array(new_l))

	return np.array(new_data)

def _parseToSparse_numpy(data):

	new_data = np.array(data)
	new_data -= 21

	return new_data


class MusicPlayer():
	'''
		(W)rap the music player in a class
	'''
	def __init__(self,):

		freq = 44100    # audio CD quality
		bitsize = -16   # unsigned 16 bit
		channels = 2    # 1 is mono, 2 is stereo
		buffer = 1024   # number of samples
		pygame.mixer.init(freq, bitsize, channels, buffer)

	def _garnSample(self,sample):
		'''
			take the GARN's sample, make it a midi
			returns the midi file
		'''
		return True

	def play_music(self,music_file):
		"""
		stream music with mixer.music module in blocking manner
		(no skratsa skroutsa)
		this will stream the sound from disk while playing
		"""
		pygame.mixer.music.load(music_file)
		clock = pygame.time.Clock()
		pygame.mixer.music.play()
		# check if playback has finished
		while pygame.mixer.music.get_busy():
			clock.tick(30)