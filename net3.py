import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT
# from theano.tensor.shared_randomstreams import RandomStreams
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
srng = RandomStreams(seed=0xbeef)

from ops import batchnorm

class Net():
	def __init__(self, indim,outdim,hdim,hdim1,hdim2,
					hdim3, b_size, scale = .1, scale_b = .3):
		self.inp_s = indim
		self.out_s = outdim
		self.hid_s = hdim
		self.hid_s1 = hdim1
		self.hid_s2 = hdim2
		self.hid_s3 = hdim3
		self.batch_size = b_size

		self.weights_ih = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.inp_s)
				-1,
				1,
				(self.inp_s, self.hid_s)
				).astype(theano.config.floatX),
				name="weights_ih"
			)

		self.weights_hh1 = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s, self.hid_s1)
				).astype(theano.config.floatX),
				name="weights_ho"
			)
		self.weights_h1h2 = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s1, self.hid_s2)
				).astype(theano.config.floatX),
				name="weights_ho"
			)
		self.weights_h2h3 = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s2, self.hid_s3)
				).astype(theano.config.floatX),
				name="weights_ho"
			)
		self.weights_h3o = theano.shared(
			scale*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s3, self.out_s)
				).astype(theano.config.floatX),
				name="weights_ho"
			)

		self.weights_bias_ih = theano.shared(
			scale_b*np.ones((b_size,self.hid_s)
				).astype(theano.config.floatX),
				name="weights_bias_ho"
			)

		self.weights_bias_hh1 = theano.shared(
			scale_b*np.ones((b_size,self.hid_s1)
				).astype(theano.config.floatX),
				name="weights_bias_ho"
			)
		self.weights_bias_h1h2 = theano.shared(
			scale_b*np.ones((b_size,self.hid_s2)
				).astype(theano.config.floatX),
				name="weights_bias_ho"
			)
		self.weights_bias_h2h3 = theano.shared(
			scale_b*np.ones((b_size,self.hid_s3)
				).astype(theano.config.floatX),
				name="weights_bias_ho"
			)
		self.weights_bias_h3o = theano.shared(
			scale_b*np.ones((b_size,self.out_s)
				).astype(theano.config.floatX),
				name="weights_bias_ih"
			)

		self.lr = TT.scalar(name="learning rate")
		self.E = TT.scalar(name="Error")
		self.inp = TT.matrix(name="input")
		self.target = TT.matrix(name="target")

	def set_params(self,):
		self.params =[
                        self.weights_ih,
						self.weights_bias_ih,
						self.weights_hh1,
						self.weights_bias_hh1,
						self.weights_h1h2,
						self.weights_bias_h1h2,
						self.weights_h2h3,
						self.weights_bias_h2h3,
						self.weights_h3o,
						self.weights_bias_h3o
                    ]

	def step(self, vin,dropout=False):

		inp = vin

		if dropout:
			p = 0.5
		else:
			p = 1.

		h = TT.nnet.relu(
			TT.dot(inp,self.weights_ih)+self.weights_bias_ih
			)
		# h = batchnorm(h)
		h*=(srng.binomial(size=(h.shape),p=0.5).astype(theano.config.floatX))

		h1 = TT.nnet.relu(
			TT.dot(h,self.weights_hh1)+self.weights_bias_hh1
			)

		# h1 = batchnorm(h1)
		h1*=srng.binomial(size=(h1.shape),p=0.5).astype(theano.config.floatX)

		h2 = TT.nnet.relu(
			TT.dot(h1,self.weights_h1h2)+self.weights_bias_h1h2
			)
		# h2 = batchnorm(h2)
		h2*=srng.binomial(size=(h2.shape),p=0.5).astype(theano.config.floatX)

		h3 = TT.nnet.relu(
			TT.dot(h2,self.weights_h2h3)+self.weights_bias_h2h3
			)
		# h3 = batchnorm(h3)
		h3*=srng.binomial(size=(h3.shape),p=0.5).astype(theano.config.floatX)

		oa = TT.nnet.sigmoid(
			TT.dot(h3,self.weights_h3o)+self.weights_bias_h3o
			)

		return oa
	def step_f(self,):
		output = self.step(self.inp)

		self.stepper = theano.function(
			inputs=[self.inp],
			outputs=output)

		return self.stepper

	def train_f(self,):
		output = self.step(self.inp)
		normalisationTerm = (self.weights_ho.norm(L=1)+self.weights_ih.norm(L=1))
		self.E = TT.mean((self.target - output)**2)


		self.params =[
                        self.weights_ih,
						self.weights_bias_ih,
						self.weights_hh1,
						self.weights_bias_hh1,
						self.weights_h1h2,
						self.weights_bias_h1h2,
						self.weights_h2h3,
						self.weights_bias_h2h3,
						self.weights_h3o,
						self.weights_bias_h3o
                    ]
		self.params = params

		gparams = TT.grad(self.E+normalisationTerm, params)
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E+normalisationTerm, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])

		updates = [
			(param, param - self.lr * gparam)
			for param, gparam in zip(params, gparams)
		]

		self.trainer =  theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[self.E+normalisationTerm],
			updates= updates
			)

		return self.trainer

if __name__ =="__main__":
	import matplotlib.pyplot as plt

	data = np.random.normal(size=(11000,230))
	b_size = 30
	epochs = 100


	print "Compiling.."
	n = Net(230,230,450,b_size)
	n.step_f()
	n.train_f()

	print "Testing.."
	errors = []
	for e in range(epochs):
		np.random.shuffle(data)
		for b_i in range(len(data)/b_size):
			inp = data[b_i*b_size: (b_i+1)*b_size]
			out = n.stepper(inp)

			error = ((out-inp)**2).mean()


			er = n.trainer(inp,out[0],0.01)
			errors.append(er[0])
		print "batch ",b_i," error ", error," epoch ",e

		# plt.plot(errors)
		# plt.show()
