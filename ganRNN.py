import os
import numpy as np
from numpy import linalg as LA
import math
from sklearn.externals import joblib

import theano
import theano.tensor as TT

from theano.tensor.shared_randomstreams import RandomStreams
SRNG = RandomStreams(seed=0xbeef)

from updates import *
from updates_v2 import *
from ops import batchnorm

class GAN(object):
    def __init__(self,G,D):
        # net for generator
        self.generator = G
        self.generator.set_params()
        self.gstep = G.step

        # net for discriminator
        self.discriminator = D
        self.discriminator.set_params()
        self.dstep = D.step

        # model variables
        self.x = TT.matrix(name="x")
        self.z = TT.matrix(name="z")
        self.z1 = TT.matrix(name="z1")
        self.lr = TT.scalar(name="learning rate")

        self.desc = "ganRNN"

    def save(self, epochs=0):
        model_dir = 'model/%s'%self.desc
        
        if not os.path.exists(model_dir):
            os.makedirs(model_dir)

        joblib.dump([p.get_value() for p in self.generator.params],
            'model/%s/%d_gen_params.jl'%(self.desc, epochs))

        joblib.dump([p.get_value() for p in self.discriminator.params],
            'model/%s/%d_disc_params.jl'%(self.desc, epochs))
        

    def stepG(self):
        '''
            Compiled fast feed forward pass of the Generator
        '''
        ret,up = self.gstep(self.z)
        self.stepperG = theano.function(inputs=[self.z],
                                        outputs=ret,
                                        updates = up,
                                        allow_input_downcast=True)


    def stepD(self):
        '''
            Compiled fast feed forward pass of the Discriminator
        '''
        ret,up = self.dstep( self.x )
        self.stepperD = theano.function(inputs=[self.x],
                                        outputs=ret,
                                        updates=up,
                                        allow_input_downcast=True)

    def trainD(self,):

        # all outputs here should be one
        od,up = self.dstep(self.x)
        # like this
        od_l = TT.ones(od.shape,
				dtype=theano.config.floatX)

        # all outputs here should be zero
        og,up1 = self.gstep(self.z)
        odg,up2 = self.dstep(og)
        # like this
        odg_l = TT.zeros(odg.shape,
				dtype=theano.config.floatX)

        # accumulate the error
        # d_er = TT.nnet.categorical_crossentropy(od, od_l)
        # d_er += TT.nnet.categorical_crossentropy(odg, odg_l)

        d_er = -.5*(od_l-od)*TT.log(od) -\
                .5*(odg_l-odg)*TT.log(1.-odg)


        params = self.discriminator.params

        # maybe we want sparse entries in the weights
        # forcing a kind of "feature selection"
        normalisationTerm = TT.sum([p.norm(L=2) for p in params])

        # calculate the gradients
        # gparams = TT.grad(
        #             TT.mean(d_er),
        #             params,
        #             disconnected_inputs="warn"
        #             )

        # adam optimizer
        # updates = adam(params , gparams , self.lr, beta1=0.5)RMSprop
        # updater = Adam(lr=self.lr, b1=0.5, regularizer=Regularizer(l2=2.5e-5))
        updater = RMSprop(lr=self.lr, regularizer=Regularizer(l2=2.5e-5))
        # updater = SGD(lr=self.lr, regularizer=Regularizer(l2=2.5e-5))
        updates = updater(params, -TT.mean(d_er))
        # compile and done


        self.trainerD = theano.function(inputs=[self.x,self.z,self.lr],
                                outputs=d_er,
                                updates=updates + up + up1 + up2,
                                # on_unused_input='warn'
                                allow_input_downcast=True
                                )


        return self.trainerD


    def trainG(self):

        # all outputs here should be ones
        og,up = self.gstep(self.z1)
        odg,up1 = self.dstep(og)
        # like this
        odg_l = TT.ones(odg.shape,
				dtype=theano.config.floatX)
        # g_er = TT.nnet.categorical_crossentropy(odg, odg_l)
        # g_er = -.5*(odg_l-odg)*TT.log(odg)

        sig_der = TT.nnet.sigmoid(odg)*(1-TT.nnet.sigmoid(odg))
        g_er = -.5*(odg_l-odg)*TT.exp(sig_der**(-1))

        params = self.generator.params


        normalisationTerm = TT.sum([p.norm(L=2) for p in params])
        # gparams = TT.grad(
        #                 TT.mean(g_er),
        #                  params)


        # updates = adam(params , gparams , self.lr, beta1=0.5)
        # updater = Adam(lr=self.lr, b1=0.5, regularizer=Regularizer(l2=2.5e-5))
        updater = RMSprop(lr=self.lr, regularizer=Regularizer(l2=2.5e-5))
        # updater = SGD(lr=self.lr, regularizer=Regularizer(l2=2.5e-5))
        updates = updater(params, -TT.mean(g_er))

        # compile and done
        self.trainerG =  theano.function(inputs=[self.z1,self.lr],
                                outputs=g_er,
                                updates=updates + up + up1,
                                allow_input_downcast=True)

        return self.trainerG
