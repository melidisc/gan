import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

class Net():
	def __init__(self, indim,outdim,hdim):
		self.inp_s = indim
		self.out_s = outdim
		self.hid_s = hdim

		self.weights_ih = theano.shared(
			.1*np.random.uniform(#np.sqrt(2.0/self.inp_s)
				-1,
				1,
				(self.inp_s, self.hid_s)
				).astype(theano.config.floatX)
			)

		self.weights_ho = theano.shared(
			.1*np.random.uniform(#np.sqrt(2.0/self.hid_s)
				-1,
				1,
				(self.hid_s, self.out_s)
				).astype(theano.config.floatX)
			)
		self.weights_bias_ho = theano.shared(
			np.ones(self.out_s
				).astype(theano.config.floatX)
			)
		self.weights_bias_ih = theano.shared(
			np.ones(self.hid_s
				).astype(theano.config.floatX)
			)

		self.lr = TT.scalar(name="learning rate")
		self.inp = TT.vector(name="input")
		self.target = TT.vector(name="target")
        self.params = [self.weights_ih,self.weights_ho,self.weights_bias_ih,seld.weights_bias_ho]

	def step(self, vin):

		inp = vin

		ha = TT.tanh(
			TT.dot(inp,self.weights_ih)+self.weights_bias_ih
			)
		oa = TT.tanh(TT.dot(ha,self.weights_ho)+self.weights_bias_ho)

		return oa
	def step_f(self,):
		output = self.step(self.inp)

		return theano.function(
			inputs=[self.inp],
			outputs=[output])
	def train_f(self,):

		output = self.step(self.inp)
		normalisationTerm = (self.weights_ho.norm(L=1)+self.weights_ih.norm(L=1))
		E = TT.sqrt(TT.mean((self.target - output)**2))

		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E+normalisationTerm, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
		# gW_ih, gW_bih, gW_ho, gW_bho = TT.grad(E, [self.weights_ih,self.weights_bias_ih, self.weights_ho, self.weights_bias_ho])
        gparams = TT.grad(E, self.params)

        updates = [
        (param, param - self.lr * gparam)
        for param, gparam in zip(self.params, gparams)
        ]
		return theano.function(
			inputs=[self.inp, self.target, self.lr],
			outputs=[E, output.flatten()],
			updates= updates
			)
